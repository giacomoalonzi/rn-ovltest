// @flow

import React, { Component } from 'react';
import { format } from 'date-fns'
import {StyleSheet, Text, View, SectionList} from 'react-native';
import type { Transaction, Transactions} from '../../types/list'
import ErrorBoundary from '../ErrorBoundary'

type Props = {
  transactionList: Array<Transactions>
};

export default class TransactionList extends Component<Props> {
  constructor(props) {
    super(props);
  }

  getSectionList = (list: Array<any> = []): Array<List> => {
    return list.map(item => ({
        title: item.transactionDateGroup && format(item.transactionDateGroup, 'MMMM YYYY'), 
        data: item.transactions || []
      })
    )
  }
  
  renderTransaction = ({ item }): Component => {
    return (
      <View style={styles.transaction} key={item.id}>
        <Text style={styles.transactionDescription}>{item.description}</Text>
        <View style={styles.transactionCategoryAndAmount}>
          <Text style={styles.transactionCategory}>{item.category}</Text>
          <Text style={styles.transactionAmount}>{item.amount}{item.currency}</Text>
        </View>
      </View>
    )
  }

  render() {
    return(
      <View style={styles.transactionsList}>
        <ErrorBoundary>
          <SectionList
            renderItem={this.renderTransaction}
            stickySectionHeadersEnabled={false}
            renderSectionHeader={({section: {title}}) => (
              <Text style={styles.sectionTitle}>{title}</Text>
            )}
            sections={this.getSectionList(this.props.transactionList)}
            keyExtractor={(item) => item.id}
          />
        </ErrorBoundary>
      </ View>
    )
  }
}


const styles = StyleSheet.create({
  sectionTitle: {
    paddingTop: 24,
    backgroundColor: '#F5FCFF',
    paddingBottom: 8,
    fontSize: 21,
    fontWeight: 'bold'
  },
  transactionsList: {
    paddingBottom: 32,
    paddingTop: 8
  },
  transaction: {
    paddingBottom: 16
  },
  transactionDescription: {
    fontSize: 16,
    paddingBottom: 4,
    textTransform: 'capitalize'
  },
  transactionCategoryAndAmount: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  transactionCategory: {
    textTransform: 'uppercase',
    opacity: .7,
    fontSize: 12,
    letterSpacing: 1.5
  },
  transactionAmount: {
    fontWeight: 'bold',
    marginTop: -22
  }
});
