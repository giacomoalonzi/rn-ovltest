// @flow

import React, { Component } from 'react';
import { format } from 'date-fns'
import {StyleSheet, Text, View, } from 'react-native';

export default class ErrorBoundary extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = { error: null };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({ error });
  }

  renderError = () => {
    return(
      <View style={styles.error}>
        <Text>Something went wrong</Text>
      </ View>
    )
  }

  render() {
    if(this.state.error) {
      return this.renderError()
    } else {
      return this.props.children
    }
  }
}

const styles = StyleSheet.create({
  error: {
    paddingTop: 24,
    paddingBottom: 24,
    backgroundColor: '#F5FCFF',
    fontWeight: 'bold',
    textAlign: 'center'
  },
});
