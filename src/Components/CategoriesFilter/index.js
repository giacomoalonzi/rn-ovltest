// @flow

import React, { Component } from 'react';
import {StyleSheet, Text, View, SectionList, TouchableHighlight} from 'react-native';

type Props = {
  categories: Array<string>,
  selectedCategory: String,
  onSelectCategory: Function,
};

export default class CategoriesFilter extends Component<Props> {
  constructor(props) {
    super(props);
  }
  onPress = (categoryName: string): void => () =>{
    this.props.onSelectCategory(categoryName)
  }

  onReset = (): void => {
    this.props.onSelectCategory('')
  }

  renderCategory = (categoryName: string, index: number): Component => {
    return (
      <TouchableHighlight 
        onPress={this.onPress(categoryName)} 
        style={[
          styles.singleCategory, 
          categoryName == this.props.selectedCategory && styles.singleCategoryActive
        ]} 
        key={index}>
          <Text>{categoryName}</Text>
      </TouchableHighlight>
    )
  }

  render() {
    return(
      <View style={styles.wrapContainer}>
        <View style={styles.categoriesContainer}>
        {  this.props.categories && this.props.categories.map((categoryName, index) => this.renderCategory(categoryName, index)) }
        </View>
        <View style={{flex: 1}}>
          <TouchableHighlight 
            style={[styles.singleCategory, !this.props.selectedCategory && styles.resetButton ]}
            onPress={this.onReset}
          >
            <Text>Reset Filter</Text>
          </TouchableHighlight>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  wrapContainer: {
    flex: 1,
    width: '100%',
  },
  categoriesContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flex: 1
  },
  singleCategory: {
    flex: 1,
    backgroundColor: '#ccc',
    paddingTop: 24,
    paddingBottom: 24,
    flexDirection: 'row',
    justifyContent:'center',
  },
  resetButton: {
    opacity: 0.5,
  },
  singleCategoryActive: {
    backgroundColor: '#ddd'
  }
});
