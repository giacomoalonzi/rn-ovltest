// @flow
import React, { Component } from 'react';
import {StyleSheet, TextInput, View} from 'react-native';

type Props = {
  onSearch: Function
};

export default class InputSearch extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = { 
      onSearch: undefined,
    };
  }
  onChangeText = (text) => {
    this.props.onSearch(text)
  }
  render() {
    return(
      <View>
        <TextInput
          style={styles.textInput}
          onChangeText={this.onChangeText}
          placeholder={'Search for description...'}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  textInput: {
    height: 40, 
    borderColor: 'gray', 
    borderWidth: 1,
    marginBottom: 8,
    paddingLeft: 8,
    paddingRight: 8
  },
});
