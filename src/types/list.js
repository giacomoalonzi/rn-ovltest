// @flow

export type Transaction = {
  id: string,
  category: string,
  date: string,
  id: number,
  description: string,
  amount: number,
  currency: string
}

export type Transactions = {
  title: string,
  data: Array<Transaction>
}