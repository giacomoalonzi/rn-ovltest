/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {TouchableHighlight} from  'react-native';
import React from 'react';
import ReactDOM from 'react-dom'
import CategoriesFilter from '../src/Components/CategoriesFilter';

import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

it('renders without any selected category', () => {
  const snap = renderer.create(<CategoriesFilter />).toJSON()
  expect(snap).toMatchSnapshot();
});

it('renders without a selected category', () => {
  const snap = renderer.create(<CategoriesFilter categories={['shopping', 'house']} />).toJSON()
  expect(snap).toMatchSnapshot();
});

it('should invoke onPress prop when pressed', function () {
    const onPressEvent = jest.fn();
    onPressEvent.mockReturnValue('Link on press invoked');
    const wrapper = shallow(<CategoriesFilter onSelectCategory={onPressEvent} />)
    wrapper.find(TouchableHighlight).first().props().onPress();
    expect(onPressEvent.mock.calls.length).toBe(1);
})