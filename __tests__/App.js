import 'react-native';
import React from 'react';
import App from '../App';
import renderer from 'react-test-renderer';


it('renders correctly', () => {
  const snap = renderer.create(<App />).toJSON()
  expect(snap).toMatchSnapshot();
});

it('should extract month and year from the passed value', () => {
  const wrapper = renderer.create(<App />).getInstance()
  expect(wrapper.getMonthAndYear('2018-07-16T03:54:54'))
   .toEqual('2018-07')     
});

const mockDateForCategoriesExtractTest = [
  {
    "id": 16,
    "description": "sint mollit enim",
    "amount": 396,
    "date": "2018-07-16T03:14:05",
    "currency": "£",
    "category": "travelling"
  },
  {
    "id": 17,
    "description": "exercitation nisi enim",
    "amount": 721,
    "date": "2019-01-13T03:13:22",
    "currency": "£",
    "category": "shopping"
  },
  {
    "id": 18,
    "description": "mollit non consequat",
    "amount": 784,
    "date": "2018-03-19T07:26:59",
    "currency": "£",
    "category": "housing"
  },
]

it('should extract all categories from given file', () => {
  const wrapper = renderer.create(<App />).getInstance()
  expect(wrapper.getAllCategories(mockDateForCategoriesExtractTest))
   .toEqual(['travelling','shopping','housing'])     
});

const mockOrderDataByDateTest = [
  {
    "id": 16,
    "description": "sint mollit enim",
    "amount": 396,
    "date": "2018-07-16T03:14:05",
    "currency": "£",
    "category": "travelling"
  },
]

const mockOrderDataByDateTestExpected = [
  {
    transactionDateGroup: '2018-07',
    transactions: [
      {
        "id": 16,
        "description": "sint mollit enim",
        "amount": 396,
        "date": "2018-07-16T03:14:05",
        "currency": "£",
        "category": "travelling"
      },
    ]
  }
]

it('should return an array with ordered data collection', () => {
  const wrapper = renderer.create(<App />).getInstance()
  expect(wrapper.orderDataByDate(mockOrderDataByDateTest))
   .toEqual(mockOrderDataByDateTestExpected)     
});

const mockFilterDataByCategoryTest = [
  {
    transactionDateGroup: '2018-07',
    transactions: [
      {
        "id": 16,
        "description": "sint mollit enim",
        "amount": 396,
        "date": "2018-07-16T03:14:05",
        "currency": "£",
        "category": "travelling"
      },
      {
        "id": 30,
        "description": "aliqua culpa voluptate",
        "amount": 815,
        "date": "2018-05-16T03:23:24",
        "currency": "£",
        "category": "housing"
      },
      {
        "id": 18,
        "description": "sint mollit enim",
        "amount": 396,
        "date": "2018-07-16T03:14:05",
        "currency": "£",
        "category": "travelling"
      },
    ]
  },
  {
    transactionDateGroup: '2018-08',
    transactions: [
      {
        "id": 16,
        "description": "sint mollit enim",
        "amount": 396,
        "date": "2018-07-16T03:14:05",
        "currency": "£",
        "category": "travelling"
      },
      {
        "id": 25,
        "description": "culpa eiusmod duis",
        "amount": 820,
        "date": "2018-09-13T12:24:24",
        "currency": "£",
        "category": "housing"
      },
    ]
  }
]
const mockFilterDataByCategoryTestExpect = [
  {
    transactionDateGroup: '2018-07',
    transactions: [
      {
        "id": 30,
        "description": "aliqua culpa voluptate",
        "amount": 815,
        "date": "2018-05-16T03:23:24",
        "currency": "£",
        "category": "housing"
      },
    ]
  },
  {
    transactionDateGroup: '2018-08',
    transactions: [
      {
        "id": 25,
        "description": "culpa eiusmod duis",
        "amount": 820,
        "date": "2018-09-13T12:24:24",
        "currency": "£",
        "category": "housing"
      },
    ]
  }
]

it('should return a filtered collection of data by categories', () => {
  const wrapper = renderer.create(<App />).getInstance()
  expect(wrapper.filterTransactionsOnCategorySelection('housing', mockFilterDataByCategoryTest))
   .toEqual(mockFilterDataByCategoryTestExpect)     
});


const mockFilterDataBySearchTest = [
  {
    transactionDateGroup: '2018-07',
    transactions: [
      {
        "id": 16,
        "description": "sint mollit enim",
        "amount": 396,
        "date": "2018-07-16T03:14:05",
        "currency": "£",
        "category": "travelling"
      },
      {
        "id": 30,
        "description": "aliqua culpa voluptate",
        "amount": 815,
        "date": "2018-05-16T03:23:24",
        "currency": "£",
        "category": "housing"
      },
      {
        "id": 18,
        "description": "sint mollit enim",
        "amount": 396,
        "date": "2018-07-16T03:14:05",
        "currency": "£",
        "category": "travelling"
      },
    ]
  }
]

const mockFilterDataBySearchTestExpect = [
  {
    transactionDateGroup: '2018-07',
    transactions: [
      {
        "id": 16,
        "description": "sint mollit enim",
        "amount": 396,
        "date": "2018-07-16T03:14:05",
        "currency": "£",
        "category": "travelling"
      },
      {
        "id": 18,
        "description": "sint mollit enim",
        "amount": 396,
        "date": "2018-07-16T03:14:05",
        "currency": "£",
        "category": "travelling"
      },
    ]
  }
]
it('should return a filtered collection of data by search', () => {
  const wrapper = renderer.create(<App />).getInstance()
  expect(wrapper.filterTransactionBySearch('mollit', mockFilterDataBySearchTest))
   .toEqual(mockFilterDataBySearchTestExpect)     
});