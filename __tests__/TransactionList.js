/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import 'react-native';
import React from 'react';
import ReactDOM from 'react-dom'
import TransactionList from '../src/Components/TransactionList';

import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });

const mockData = [{
  transactionDateGroup: '2019-02',
  transactions: [
    {
      amount: 654,
      category: "shopping",
      currency: "£",
      date: "2019-02-23T07:58:40",
      description: "qui anim Lorem",
      id: 23
    }
  ]}
]

const mockDataModified = [{
  title: 'February 2019',
  data: [
    {
      amount: 654,
      category: "shopping",
      currency: "£",
      date: "2019-02-23T07:58:40",
      description: "qui anim Lorem",
      id: 23
    }
  ]}
]

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

it('renders without any data', () => {
  const snap = renderer.create(<TransactionList />).toJSON()
  expect(snap).toMatchSnapshot();
});

it('renders without a selected category', () => {
  const snap = renderer.create(<TransactionList transactionList={mockData} />).toJSON()
  expect(snap).toMatchSnapshot();
});

it('should manipolate object structure for sectionList', () => {
  const wrapper = renderer.create(<TransactionList />).getInstance()
   expect(wrapper.getSectionList(mockData)).toEqual(mockDataModified)     
});
