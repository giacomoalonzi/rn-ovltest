/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import ovalDataImported from './data.json'

import type { Transactions} from './types/list'
import React, {Component} from 'react';
import { format } from 'date-fns'
import { groupBy, map } from 'lodash'
import InputSearch from './src/Components/InputSearch'
import TransactionList from './src/Components/TransactionList'
import CategoriesFilter from './src/Components/CategoriesFilter'
import {Platform, StyleSheet, Text, View, SectionList} from 'react-native';

function sortByDate(data: array<any>): array<any> {
  return data.sort((a, b) => new Date(b.date) - new Date(a.date))
}

const ovalData = sortByDate(ovalDataImported)

type Props = {};
export default class App extends Component<Props> {
  state = {
    categories: [],
    orderedTransactions: [],
    filteredTransactions: [],
    selectedCategory: ''
  }

  constructor(props) {
    super(props);
    this.state = { 
      categories: this.getAllCategories(ovalData),
      orderedTransactions: this.orderDataByDate(ovalData),
      filteredTransactions: [],
    };
  }

  componentDidMount() {
    this.setState({
      filteredTransactions: this.state.orderedTransactions
    })
  }

  getMonthAndYear = (date: string): string => {
    const splitDate = date.split('T')[0]
    return `${splitDate.split('-')[0]}-${splitDate.split('-')[1]}`
  }

  getAllCategories = (data: Object): string => {
    return data.reduce(function(acc, element) {
      return !acc.includes(element.category) ? [...acc, element.category] : [...acc]
    }, [])
  }

  orderDataByDate = (items: any) :Array<Transactions> =>  {
    const transactionsGrouped = groupBy(items, item => this.getMonthAndYear(item.date));
    return map(transactionsGrouped, (transactionsGroup, transactionDate) => ({
      transactionDateGroup: transactionDate,
      transactions: transactionsGroup
    }))
    //return Object.assign(acc, { [this.getMonthAndYear(item.date)]:( acc[this.getMonthAndYear(item.date)] || [] ).concat({...item}) })
  }

  filterTransactionsOnCategorySelection = (category: string, transactionsToFilter: Array<Transaction>): Array<Transaction>=> {
    return transactionsToFilter.map(transactionsGroup => {
      const transactions = transactionsGroup.transactions.filter(transaction => transaction.category === category || !category.length)
      return transactions.length > 0 ? Object.assign({
        transactionDateGroup: transactionsGroup.transactionDateGroup,
        transactions
      }) : {}
    }).filter(trans =>Object.keys(trans).length !== 0)
  }

  filterTransactionBySearch = (searchText: string, transactionsToFilter: Array<Transaction>): Array<Transaction> => {
    return transactionsToFilter.map(transactionsGroup => {
      const transactions = transactionsGroup.transactions.filter(transaction => transaction.description.includes(searchText.toLowerCase()))
      return transactions.length > 0 ? Object.assign({
        transactionDateGroup: transactionsGroup.transactionDateGroup,
        transactions
      }) : {}
    }).filter(trans =>Object.keys(trans).length !== 0)
  }

  onSelectCategory = (category: string): string => {
    this.setState({
      selectedCategory: category
    })
  }
  componentDidUpdate(prevProps, prevState): void {
    // watch property
    if (prevState.selectedCategory !== this.state.selectedCategory) {
      this.setState({
        filteredTransactions: this.filterTransactionsOnCategorySelection(this.state.selectedCategory, this.state.orderedTransactions)
      })
    }
  }
  onSearch = (text: string): void => {
    this.setState({
      selectedCategory: '',
      filteredTransactions: this.filterTransactionBySearch(text, this.state.orderedTransactions)
    })
  }
  render() {
    return (
      <View style={styles.container}>
        <View>
          <InputSearch onSearch={this.onSearch} />
        </View>
        <View style={styles.categoryFilterContainer}>
          <CategoriesFilter 
            onSelectCategory={this.onSelectCategory} 
            selectedCategory={this.state.selectedCategory} 
            categories={this.state.categories}
          />
        </View>
        <View style={styles.transactionListContainer}>
          <TransactionList transactionList={this.state.filteredTransactions} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
    paddingLeft: 24,
    paddingRight: 24,
    flex: 1,
    backgroundColor: '#F5FCFF',
    
  },
  categoryFilterContainer: {
    height: 140,
  },
  transactionListContainer: {
    flex: 1
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
